package com.example.canvasdemo;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.os.Bundle;
import android.widget.ImageView;

import androidx.appcompat.app.AppCompatActivity;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // setup our system for drawing (get your tools ready!)
        // ------------------

        // 1. setup the frame
        ImageView imageView = findViewById(R.id.imageView);
        Bitmap b = Bitmap.createBitmap(300, 500, Bitmap.Config.ARGB_8888);

        // 2. setup the canvas
        Canvas canvas = new Canvas(b);

        // 3. setup your paintbrush
        Paint paintbrush = new Paint();


        // draw some stuff on the canvas
        // ------------------
        // 1. Set the background color
        canvas.drawColor(Color.BLACK);

        // Get RED Color
        paintbrush.setColor(Color.RED);

        // 2. draw a straight line
        canvas.drawLine(10, 50,200, 50, paintbrush);

        // Get Yellow Color
        paintbrush.setColor(Color.YELLOW);
        // Draw a diagonal line
        canvas.drawLine(10, 50,200, 150, paintbrush);

        //DRAW SOME SQUARE
        // Get Green color
        paintbrush.setColor(Color.GREEN);
        // Draw a small Rectangle
        canvas.drawRect(100, 100, 120, 120, paintbrush);


        // Get Blue Color
        paintbrush.setColor(Color.BLUE);
        // Draw a big Rectangle
        canvas.drawRect(150, 150, 200, 200, paintbrush);



        // 1. Set the text size
        paintbrush.setTextSize(40);

        // 2. Draw the text on the screen
        canvas.drawText("Hey Yo man!", 10, 400, paintbrush);



        // 1. Set the text size
        paintbrush.setTextSize(20);
        // 2. Change Color
        paintbrush.setColor(Color.WHITE);
        // 3. Draw the text on the screen
        canvas.drawText("Go Yo man!", 10, 450, paintbrush);






        // put the canvas into the frame
        // ------------------
        imageView.setImageBitmap(b);


    }
}